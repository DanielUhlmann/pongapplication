package model;

import static model.Model.*;

public class Ball {

    private double x;
    private double y;
    private int ball_height = 20;
    private int ball_width = 20;
    private int ballXSpeed;
    private int ballYSpeed;

    public Ball(int x, int y, int ballXSpeed, int ballYSpeed) {
        this.x = x;
        this.y = y;
        this.ballXSpeed = ballXSpeed;
        this.ballYSpeed = ballYSpeed;

    }
    public void update(long elapsedTime) {

        x += ballXSpeed;
        y += ballYSpeed;

        if (x < 0) {
            increaseScoreP2();
            x = WIDTH / 2;
            ballXSpeed = -ballXSpeed;
        } else if (x > WIDTH - ball_width - 2) {
            increaseScoreP1();
            x = WIDTH / 2;
            ballXSpeed = -ballXSpeed;
        } else if (y < 0 || y > HEIGHT - ball_height) {
            ballYSpeed = -ballYSpeed;
        }

    }

    public int getBall_height() {
        return ball_height;
    }

    public int getBall_width() {
        return ball_width;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public int getBallXSpeed() {

        return ballXSpeed;
    }

    public int getBallYSpeed() {
        return ballYSpeed;
    }

    public void setBallXSpeed(int ballXSpeed) {
        this.ballXSpeed = ballXSpeed;
    }

    public void setBallYSpeed(int ballYSpeed) {
        this.ballYSpeed = ballYSpeed;
    }
}
