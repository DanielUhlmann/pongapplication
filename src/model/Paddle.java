package model;

public class Paddle {
    private int x;
    private int y;

    public Paddle(int x, int y) {
        this.x = x;
        this.y = y;

    }
    public void moveUP(int dY) {
        this.y -= dY;
    }

    public void moveDOWN(int ya) {
        this.y += ya;
    }

    public int getX() {
        return x - 15;
    }

    public int getY() {
        return y;
    }

    public int getHeight() {

        return 100;
    }

    public int getwidth() {

        return 15;
    }
}
